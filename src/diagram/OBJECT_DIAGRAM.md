## Diagrama de objetos

Um diagrama de objetos UML representa uma instância específica de um diagrama de classes 
em um determinado momento. Quando representado visualmente, você verá muitas 
semelhanças ao [diagrama de classes](CLASS_DIAGRAM.md).

### Elementos de diagramas de objetos

Diagramas de objetos são fáceis de criar: são feitos de objetos, representados por retângulos 
e ligados entre si por linhas. Confira os principais elementos de um diagrama de objetos.

#### Objetos

Objetos são instâncias de uma classe.

#### Títulos de classe

Títulos de classe são os atributos específicos de uma determinada classe.

#### Atributos de classe

Atributos de classe são representados por um retângulo com duas abas que indicam um elemento de software.

#### Links

Ligações são as linhas que conectam duas formas de um diagrama de objetos, uma a outra.

## Diagrama

```plantuml
@startuml
object o1
object o2
diamond dia
object o3

o1  --> dia
o2  --> dia
dia --> o3
@enduml
```
//TODO: Realizar a criação do diagrama

## Referências

- [O que é um diagrama de objetos em UML?](https://www.lucidchart.com/pages/pt/o-que-e-diagrama-de-objetos-uml)
