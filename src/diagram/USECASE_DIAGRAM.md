## Diagrama de caso de uso

Esse documento é um diagrama de caso de uso que resume os detalhes dos usuários do 
[educacode-api](https://gitlab.com/educa-code-labs/dev/back-end/projects/educacode-api). 
Além disso, também e mostrado a interações deles com o sistema. Para criar esse diagrama 
foi usado a linguagem de modelagem unificada ([UML](https://www.ateomomento.com.br/diagramas-uml/)).

## Elementos do diagrama de caso de uso

Para entender o que é um diagrama de caso de uso, é necessário primeiro saber como é ele feito. São componentes comuns:

- **Atores**: os usuários que interagem com o sistema. Ator pode ser uma pessoa, organização ou sistema externo que interage com seu aplicativo ou sistema.
- **Sistema**: uma sequência específica de ações e interações entre os atores e o sistema. O sistema também pode ser chamado de cenário.
- **Metas**: o resultado da maioria dos casos de uso. Um diagrama criado corretamente deve descrever as atividades e variantes usadas para atingir a meta.

## Diagrama da autenticação

Este diagrama representa a herança entre os casos de uso. A autenticação é o caso de uso pai. 
Ou seja, os demais caso de uso vão herdada essas funções.

```plantuml
@startuml
left to right direction

actor "Novo estudante"  as new_student
actor "Estudante"       as student
actor "Sigaa Service"   as sigaa_service

rectangle "educacode-api auth" {

  usecase "Cadastro"                        as UC_registration
  usecase "Registro personalizado"          as UC_custom_registration
  usecase "Enviar foto de perfil"           as UC_send_profile_picture
  usecase "Validar informações"             as UC_validate
  usecase "Buscar informações\nno Sigga"    as UC_check_sigaa_service
  usecase "Enviar e-mail\n para o usuário"  as UC_send_email
  
  usecase "Login"                                   as UC_login
  usecase "Verificar dispositivo \ne credenciais"   as UC_check_device
  usecase "Bloquear conta"                          as UC_lock_account
  usecase "Gerar token JWT"                         as UC_generate_token
  
  note "Registro com informações extras de estudantes\nde faculdades que são parceiras"             as N2
  note "Pesquisa informações públicas\n para confirmar que o usuário pertence\n ao corpo docente"   as N3
  note "Se houver 5 tentativas de \nautenticação inválidas em um \nperíodo de 1 hora"               as N4
}

new_student --> UC_registration

UC_registration .. N2
N2 .. UC_custom_registration : <<include>>

UC_registration .. UC_send_profile_picture : <<include>>
UC_registration .. UC_validate : <<extend>>
UC_validate .. UC_send_email : <<include>>

UC_validate .. N3
N3 .. UC_check_sigaa_service : <<include>>
UC_check_sigaa_service .. sigaa_service

student --> UC_login

UC_login .. UC_check_device : <<extend>>
UC_check_device .. UC_generate_token : <<include>>

UC_login .. N4
N4 .. UC_lock_account : <<include>>

@enduml
```

Veja abaixo uma breve descrição de como funciona o fluxo de registro:

| Caso de uso básico  | Caso de uso relacionado     | Descrição                                                                                                                                                |
|---------------------|-----------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------|
| Cadastro            | Registro personalizado      | Esta é uma ação opcional que só ocorre caso o usuário opte por se cadastrar vinculado à sua faculdade.                                                   |
| Cadastro            | Enviar foto de perfil       | Se o usuário quiser, ele também pode enviar uma foto de perfil durante o cadastro. Se ele não enviar nada, uma foto padrão será definida.                |
| Cadastro            | Validar informações         | Durante o processo de validação, é feita uma análise das informações do usuário. Se estiverem corretas, o fluxo avança para a próxima fase.              |
| Validar informações | Buscar informações no Sigga | Esse processo é acionado se o usuário quiser vincular sua conta a uma faculdade. O sistema verificará se o aluno é realmente um estudante universitário. |
| Validar informações | Enviar e-mail de cadastro   | Ao finalizar o cadastro do usuário será enviado um e-mail dando as boas-vindas.                                                                          |


## Referências

- [O que é diagrama de caso de uso?](https://www.lucidchart.com/pages/pt/diagrama-de-caso-de-uso-uml)
