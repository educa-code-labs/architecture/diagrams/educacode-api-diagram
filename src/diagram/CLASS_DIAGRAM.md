## Diagrama de classe em UML

Um diagrama de classes é uma representação da estrutura e relações das classes que servem de modelo para objetos.

## Diagrama

```plantuml
@startuml
Object <|-- ArrayList

Object : equals()
ArrayList : Object[] elementData
ArrayList : size()

@enduml
```
//TODO: Realizar a criação do diagrama

## Referências

- [O que é um diagrama de classe em UML?](https://www.lucidchart.com/pages/pt/o-que-e-diagrama-de-classe-uml)
