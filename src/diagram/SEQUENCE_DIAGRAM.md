## Diagrama de sequência

Diagrama de sequência é um diagrama usado em UML, representando a sequência de processos num programa 
de computador. Como um projeto pode ter uma grande quantidade de métodos em classes diferentes, pode ser 
difícil determinar a sequência global do comportamento.

## Diagrama

```plantuml
@startuml
actor Bob #red
' The only difference between actor
'and participant is the drawing
participant Alice
participant "I have a really\nlong name" as L #99FF99
/' You can also declare:
   participant L as "I have a really\nlong name"  #99FF99
  '/

Alice->Bob: Authentication Request
Bob->Alice: Authentication Response
Bob->L: Log transaction
@enduml
```
//TODO: Realizar a criação do diagrama

## Referências

- [O que é um diagrama de sequência em UML?](https://www.lucidchart.com/pages/pt/o-que-e-diagrama-de-sequencia-uml)
