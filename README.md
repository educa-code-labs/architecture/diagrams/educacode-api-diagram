![logo](https://i.imgur.com/YUZzmXm.png)

Educacode Api Diagram
=======================================

[![Licence: MIT](https://img.shields.io/badge/Licence-MIT-green)](LICENCE)
[![Team](https://img.shields.io/badge/Team-Architecture-red)](https://gitlab.com/educa-code-labs/architecture)

* * *

Este projeto visa armazenar as informações de diagramas UML do projeto **educacode-api**. Esse projeto usa o PlantUML 
porque através dele é possível criar os seguintes tipos de diagramas:

- [Diagrama de casos de uso](src/diagram/USECASE_DIAGRAM.md)
- [Diagrama de objeto](src/diagram/OBJECT_DIAGRAM.md)
- [Diagrama de classes](src/diagram/CLASS_DIAGRAM.md)
- [Diagrama de sequência](src/diagram/SEQUENCE_DIAGRAM.md)

Para mais informações veja essa [documentação](https://plantuml.com/)

### Requisitos

Atualmente este projeto não tem nenhum requisito.

### Instalação

A maneira recomendada de instalar este projeto é seguindo estas etapas:

1. Realize o clone do projeto para a sua máquina

```shell
git clone git@gitlab.com:educa-code-labs/architecture/diagrams/educacode-api-diagram.git
```

Se você estiver usando as configurações de ambiente do [educa-code-labs](https://gitlab.com/educa-code-labs) recomendamos fazer o clone na seguinte pasta `/home/[seu-usuário]/vhost`.

2. Acessar as pastas do projeto

```shell
cd project-structure-template
make init
```

### Software stack

Esse projeto roda nos seguintes softwares:

- Git 2.33+
- Gitlab 15.4.0-pre
- PlantUML

### Changelog

Por favor, veja [CHANGELOG](CHANGELOG.md) para obter mais informações sobre o que mudou recentemente.

### Seja um dos contribuidores

Quer fazer parte desse projeto? Clique AQUI e leia [como contribuir](CONTRIBUTING.md).

## Segurança

Se você descobrir algum problema relacionado à segurança, envie um e-mail para reinangabriel1520@gmail.com em vez de usar o issue.

### Licença

Esse projeto está sob licença. Veja o arquivo [LICENÇA](LICENSE.md) para mais detalhes.
